﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace Harrods.UIAutomation.Tests
{
    [TestClass]
    public class RegistrationTests
    {
        [TestMethod]
        public void StartRegistration()
        {
            //using Chrome Driver
            var driver = new ChromeDriver();
            //driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 10);
            //driver.Manage().Timeouts().PageLoad = new TimeSpan(0, 0, 10);
            driver.Navigate().GoToUrl("https://secure.harrods.com/account/en-gb/start-registration");
            Thread.Sleep(5000);

            //In the registration page
            var startRegistrationHeader = driver.FindElement(By.ClassName("secure_header-title")).Text;
            Assert.AreEqual(startRegistrationHeader, "REGISTER");


            var email = driver.FindElement(By.Id("EmailAddress"));
            email.SendKeys("dprasangika1027@gmail.com");
            var joinRewardsOption = driver.FindElement(By.Id("registerOptionJoinRewards"));
            joinRewardsOption.Click();
            var continueButton = driver.FindElement(By.CssSelector("#main > section.secure_content.secure_content--less-space-after.secure_content--start-registration > form > div.field_submit.field_submit--2 > button"));
            continueButton.Click();

            ////When navigated to next page after clicking continue button
            //Thread.Sleep(15000);
            //var firstName = chromedriver.FindElement(By.CssSelector("#FirstName"));
            //firstName.SendKeys("Dilini");

            ////Just found out that in IE, you can click through to other sections, 
            ////but you loose connection with the driver

            //var ieDriver = new InternetExplorerDriver();
            //driver.Navigate().GoToUrl("https://secure.harrods.com/account/en-gb/start-registration");
        }
    }
}
